// Bài 1 : Xuất 3 số theo thứ tự tăng dần

function ascend() {
    var numberOne = +document.getElementById('number-one').value;
    var numberTwo = +document.getElementById('number-two').value;
    var numberThree = +document.getElementById('number-three').value;
    var result_1 = document.getElementById('result-1');
    if(numberOne > numberTwo && numberOne > numberThree && numberThree > numberTwo) {
        result_1.innerHTML = `Kết quả : ${numberTwo} < ${numberThree} < ${numberOne}`;
    } else if (numberOne > numberTwo && numberThree > numberTwo && numberThree > numberOne) {
        result_1.innerHTML = `Kết quả : ${numberTwo} < ${numberOne} < ${numberThree}`;
    } else if (numberTwo > numberOne && numberTwo > numberThree && numberThree > numberOne) {
        result_1.innerHTML = `Kết quả : ${numberOne} < ${numberThree} < ${numberTwo}`;
    } else if (numberOne > numberThree && numberOne > numberTwo && numberTwo > numberThree) {
        result_1.innerHTML = `Kết quả : ${numberThree} < ${numberTwo} < ${numberOne}`;
    } else if (numberTwo > numberOne && numberTwo > numberThree && numberOne > numberThree) {
        result_1.innerHTML = `Kết quả : ${numberThree} < ${numberOne} < ${numberTwo}`;
    } else {
        result_1.innerHTML = `Kết quả : ${numberOne} < ${numberTwo} < ${numberThree}`;
    }
}



// Bài 2 : Viết chương trình chào hỏi

function hi() {
    var select = document.getElementById('hello');
    var result_2 = document.getElementById('result-2');
    if(select.value == 'father') {
        result_2.innerHTML = `Kết quả : Xin chào Bố!`;
    } else if(select.value == 'mother') {
        result_2.innerHTML = `Kết quả : Xin chào Mẹ!`;
    } else if(select.value == 'brother') {
        result_2.innerHTML = `Kết quả : Xin chào Anh Trai!`;
    } else if (select.value == 'sister') {
        result_2.innerHTML = `Kết quả : Xin chào Em Gái!`;
    } else {
        result_2.innerHTML = `Kết quả : Xin chào Người lạ ơi!`;
    }
}



// Bài 3 : Đếm số chắn lẻ

function count() {
    var a = (+document.getElementById('number-1').value) % 2;
    var b = (+document.getElementById('number-2').value) % 2;
    var c = (+document.getElementById('number-3').value) % 2;
    var result_3 = document.getElementById('result-3');
    if (a == 0 && b == 0 && c == 0) {
        result_3.innerHTML = `Kết quả : Có 3 số chắn , có 0 số lẻ`;
    } else if (a == 0 && b == 0 & c == 1) {
        result_3.innerHTML = `Kết quả : Có 2 số chắn , có 1 số lẻ`;
    } else if (a == 0 && b == 1 & c == 0) {
        result_3.innerHTML = `Kết quả : Có 2 số chắn , có 1 số lẻ`;
    } else if (a == 1 && b == 0 && c == 0) {
        result_3.innerHTML = `Kết quả : Có 2 số chắn , có 1 số lẻ`;
    } else if (a == 0 && b == 1 && c == 1) {
        result_3.innerHTML = `Kết quả : Có 1 số chắn , có 2 số lẻ`;
    } else if (a == 1 && b == 0 && c == 1) {
        result_3.innerHTML = `Kết quả : Có 1 số chắn , có 2 số lẻ`;
    } else if (a == 1 && b == 1 && c == 0) {
        result_3.innerHTML = `Kết quả : Có 1 số chắn , có 2 số lẻ`;
    } else {
        result_3.innerHTML = `Kết quả : Có 0 số chắn , có 3 số lẻ`;
    }
}



// Bài 4 : Đoán hình tam giác

function guess() {
    var leng1 = +document.getElementById('length-one').value;
    var leng2 = +document.getElementById('length-two').value;
    var leng3 = +document.getElementById('length-three').value;
    var result_4 = document.getElementById('result-4');
    if(leng1 == leng2 && leng2 == leng3) {
        result_4.innerHTML = `Kết quả : Tam giác đều`;
    } else if (leng1 * leng1 + leng2 * leng2 == leng3 * leng3 || leng1 * leng3 + leng3 * leng3 == leng2 * leng2 || leng2 * leng2 + leng3 * leng3 == leng1 * leng3) {
        result_4.innerHTML = `Kết quả : Tam giác vuông`;
    } else if (leng1 == leng2 || leng1 == leng3 || leng2 == leng3) {
        result_4.innerHTML = `Kết quả : Tam giác cân`;
    } else {
        result_4.innerHTML = `Kết quả : Tam giác nào đó`;
    }
}



// Bài 5 : Tính ngày tháng năm

function checkMonth(numberMonth) {
    switch(numberMonth) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12: {
            return 31;
        }
        case 4:
        case 6:
        case 9:
        case 11: {
            return 30;
        }
        case 2: {
            return 28;
        }
        default: {
            return 'Tháng không xác định';
        }
    }
}

function yesterday() {
    var numberDay = +document.getElementById('number-day').value;
    var numberMonth = +document.getElementById('number-month').value;
    var numberYear = +document.getElementById('number-year').value;
    var result_5 = document.getElementById('result-5');
    var checksMonth = checkMonth(numberMonth);
    if(numberDay == 1 && numberMonth == 1) {
        result_5.innerHTML = `Kết quả : 31 / 12 / ${numberYear - 1}`;
    } else if(numberDay > 0 && numberDay <= checksMonth){
        result_5.innerHTML = `Kết quả : ${numberDay - 1} / ${numberMonth} / ${numberYear}`;
    } else {
        result_5.innerHTML = `Kết quả : Ngày không xác định`;
    }
}

function tomorrow() {
    var numberDay = +document.getElementById('number-day').value;
    var numberMonth = +document.getElementById('number-month').value;
    var numberYear = +document.getElementById('number-year').value;
    var result_5 = document.getElementById('result-5');
    var checksMonth = checkMonth(numberMonth);
    if(numberDay === checksMonth) {
        result_5.innerHTML = `Kết quả : 1 / ${numberMonth + 1} / ${numberYear}`;
    } else if(numberDay > 0 && numberDay <= checksMonth) {
        result_5.innerHTML = `Kết quả : ${numberDay + 1} / ${numberMonth} / ${numberYear}`;
    } else {
        result_5.innerHTML = `Kết quả : Ngày không xác định`;
    }
}



// Bài 6 : Tính ngày

function calculate() {
    var month = +document.getElementById('mont-number').value;
    var year = +document.getElementById('year-number').value;
    var result_6 = document.getElementById('result-6');
    switch(month) {
        case 4:
        case 6:
        case 9:
        case 11: {
            result_6.innerHTML = `Kết quả : Tháng ${month} năm ${year} có 30 ngày`;
            break;
        }
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12: {
            result_6.innerHTML = `Kết quả : Tháng ${month} năm ${year} có 31 ngày`;
            break;
        }
        case 2: {
            if((year % 4 ==0 && year % 100 != 0) || year % 400 == 0) {
                result_6.innerHTML = `Kết quả : Tháng ${month} năm ${year} có 29 ngày`;
                break;
            } else {
                result_6.innerHTML = `Kết quả : Tháng ${month} năm ${year} có 28 ngày`;
                break;
            }
        }
        default: {
            result_6.innerHTML = `Kết quả : Tháng không xác định`;
        }
    }
    return false;
}



// Bài 7 : Đọc số

function checkHundred(a) {
    var threeNumber = +document.getElementById('three-number').value;
    var a = Math.floor(threeNumber / 100);
    switch(a) {
        case 0: {
            return 'Không trăm';
        }
        case 1: {
            return 'Một trăm';
        }
        case 2: {
            return 'Hai trăm';
        }
        case 3: {
            return 'Ba trăm';
        }
        case 4: {
            return 'Bốn trăm';
        }
        case 5: {
            return 'Năm trăm';
        }
        case 6: {
            return 'Sáu trăm';
        }
        case 7: {
            return 'Bảy trăm';
        }
        case 8: {
            return 'Tám trăm';
        }
        case 9: {
            return 'Chín trăm';
        }
    }
}

function checkdozens(b) {
    var threeNumber = +document.getElementById('three-number').value;
    var b = Math.floor((threeNumber / 10) % 10);
    switch(b) {
        case 0: {
            return 'linh';
        }
        case 1: {
            return 'mười';
        }
        case 2: {
            return 'hai mươi';
        }
        case 3: {
            return 'ba mươi';
        }
        case 4: {
            return 'bốn mươi';
        }
        case 5: {
            return 'năm mươi';
        }
        case 6: {
            return 'sáu mươi';
        }
        case 7: {
            return 'bảy mươi';
        }
        case 8: {
            return 'tám mươi';
        }
        case 9: {
            return 'chín mươi';
        }
    }
}

function checkUnit(c) {
    var threeNumber = +document.getElementById('three-number').value;
    var c = threeNumber % 10;
    switch(c) {
        case 0: {
            return 'không';
        }
        case 1: {
            return 'mốt';
        }
        case 2: {
            return 'hai';
        }
        case 3: {
            return 'ba';
        }
        case 4: {
            return 'bốn';
        }
        case 5: {
            return 'năm';
        }
        case 6: {
            return 'sáu';
        }
        case 7: {
            return 'bảy';
        }
        case 8: {
            return 'tám';
        }
        case 9: {
            return 'chín';
        }
    }
}

function read() {
    var threeNumber = +document.getElementById('three-number').value;
    var result_7 = document.getElementById('result-7');
    var hundreds = checkHundred(threeNumber);
    var dozens = checkdozens(threeNumber);
    var units = checkUnit(threeNumber);
    var current = hundreds +" "+ dozens+ " " + units;
    if(threeNumber == 100) {
        result_7.innerHTML = `Kết quả : Một trăm`;  
    } else if(threeNumber > 100 && threeNumber <= 999) {
        result_7.innerHTML =  `Kết quả : ${current}`;
    }else {
        result_7.innerHTML = `Kết quả : Input không đúng yêu cầu`;
    }
}



// Bài 8 : Tìm sinh viên xa trường nhất

function find() {
    var nameStudent_1 = document.getElementById('student-1').value;
    var student_1_x = +document.getElementById('student-1-x').value;
    var student_1_y = +document.getElementById('student-1-y').value;
    var nameStudent_2 = document.getElementById('student-2').value;
    var student_2_x = +document.getElementById('student-2-x').value;
    var student_2_y = +document.getElementById('student-2-y').value;
    var nameStudent_3 = document.getElementById('student-3').value;
    var student_3_x = +document.getElementById('student-3-x').value;
    var student_3_y = +document.getElementById('student-3-y').value;
    var schools_x = +document.getElementById('schools-x').value;
    var schools_y = +document.getElementById('schools-y').value;
    var result_8 = document.getElementById('result-8');
    var student_1 = (schools_x - student_1_x) * (schools_x - student_1_x) + (schools_y - student_1_y) * (schools_y - student_1_y);
    var student_2 = (schools_x - student_2_x) * (schools_x - student_2_x) + (schools_y - student_2_y) * (schools_y - student_2_y);
    var student_3 = (schools_x - student_3_x) * (schools_x - student_3_x) + (schools_y - student_3_y) * (schools_y - student_3_y);
    if(student_1 == 0) {
        result_8.innerHTML = `Kết quả : Sinh viên xa trường nhất : ${nameStudent_1}`;
    } else if(student_2 == 0) {
        result_8.innerHTML = `Kết quả : Sinh viên xa trường nhất : ${nameStudent_2}`;
    } else if(student_3 == 0) {
        result_8.innerHTML = `Kết quả : Sinh viên xa trường nhất : ${nameStudent_3}`;
    } else if(student_1 > student_2 && student_1 > student_3) {
        result_8.innerHTML = `Kết quả : Sinh viên xa trường nhất : ${nameStudent_1}`;
    } else if(student_2 > student_1 && student_2 > student_3) {
        result_8.innerHTML = `Kết quả : Sinh viên xa trường nhất : ${nameStudent_2}`;
    } else if(student_3 > student_2 && student_3 > student_1) {
        result_8.innerHTML = `Kết quả : Sinh viên xa trường nhất : ${nameStudent_3}`;
    } else {
        result_8.innerHTML = `Kết quả : Cả 3 sinh viên cách trường bằng nhau`;
    }
}